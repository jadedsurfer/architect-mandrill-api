"use strict";
var mandrill = require("mandrill-api");

module.exports = function(options, imports, register) {
  var debug = imports.debug("mandrill");
  debug("start");

  debug("register mandrill");
  register(null, {
    mandrill: mandrill
  });
};
